FROM python:2.7.11-alpine

EXPOSE 8000

ENV GIT_SSH_KEY=
ENV GIT_USER_EMAIL="repworth_mkdocs@hub.docker.com"
ENV GIT_USER_NAME="repworth/mkdocs"

# install dependent packages
RUN apk -U add openssh git && \
rm /var/cache/apk/*

RUN pip install mkdocs==0.15.1

COPY entrypoint.sh /

ENTRYPOINT ["/entrypoint.sh"]

CMD ["/bin/sh"]