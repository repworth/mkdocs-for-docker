Docker container with: 

- [mkdocs](http://www.mkdocs.org/)

## Scripts
Apply provided GIT credentials and open shell:
```
docker \
  run \
  -it \
  -e GIT_SSH_KEY \
  -e GIT_USER_EMAIL \
  -e GIT_USER_NAME \
  repworth/mkdocs:mkdocs0.15
```