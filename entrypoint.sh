#!/bin/sh

SSH_KEY_DIR=~/.ssh
SSH_KEY_FILE_PATH=${SSH_KEY_DIR}/id_rsa

# setup git
mkdir -p ${SSH_KEY_DIR} && \
printf "%s\n" "${GIT_SSH_KEY}" > ${SSH_KEY_FILE_PATH} && \
chmod 600 ${SSH_KEY_FILE_PATH} && \
git config --global push.default simple && \
git config --global user.email ${GIT_USER_EMAIL} && \
git config --global user.name ${GIT_USER_NAME}

# start ssh-agent
eval "$(ssh-agent -s)"

exec "$@"